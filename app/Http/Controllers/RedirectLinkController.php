<?php

namespace App\Http\Controllers;

use App\Models\Link;
use App\Models\Visit;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use phpDocumentor\Reflection\Types\String_;

class RedirectLinkController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param Request $request
     * @param String_ $shortLink
     * @return RedirectResponse
     */

    public function redirect(Request $request,$shortLink):RedirectResponse
    {
        $link = Link::where('short_link', $shortLink)->first();
        if (!$link) {
            //todo implement a view for 404
            abort(Response::HTTP_NOT_FOUND);
        }
        $user_ip = $request->ip();
        $user_agent = $request->userAgent();
        Visit::create([
            'link_id' => $link->id,
            'user_ip' => $user_ip,
            'user_agent' => $user_agent
        ]);
        return response()->redirectTo($link->link);
    }

}
