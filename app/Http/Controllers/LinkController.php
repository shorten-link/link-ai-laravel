<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreLinkRequest;
use App\Http\Resources\LinkResource;
use App\Models\Link;
use App\Models\Visit;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class LinkController extends Controller
{

    public function index()
    {
        $user_id = Auth::id();
        $userLinks = Link::where('user_id',$user_id)->withCount('visits')->get();
        return LinkResource::collection($userLinks);
    }

    public function store(StoreLinkRequest $request)
    {

        $link = $request->input('link');
        $ip = $request->ip();

        $shortenedLink = Link::where('link', $link)->first();

        if ($shortenedLink) {
            return $shortenedLink;
        }

        return Link::create([
            'link' => $link,
            'ip' => $ip,
            'user_id' => auth()->id(),
            'short_link' => $this->generateShortLink(),
        ]);
    }

    private function generateShortLink(): string
    {
        do {
            $randomStr = Str::random(6);
        } while (Link::where('link', $randomStr)->exists());

        return $randomStr;
    }
}





