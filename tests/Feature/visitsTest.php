<?php

namespace Tests\Feature;

use App\Models\Link;
use App\Models\User;
use App\Models\Visit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Schema;
use Tests\TestCase;

class visitsTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_visit_database_can_store_hits()
    {
        $user = User::factory()->create();
        $link = Link::factory([
            'user_id' => $user->id
        ])->create();
        $this->call('get', route(
            'short_link', $link->short_link
        ));
        $this->assertDatabaseHas('visits', [
            'link_id' => $link->id,
            'user_ip' => '127.0.0.1',
            'user_agent' => 'Symfony'
        ]);
    }

    //test Relations ....
    public function test_a_visit_belongs_to_a_link()
    {
        $user = User::factory()->create();
        $link = Link::factory([
            'user_id' => $user->id
        ])->create();
        $visit = Visit::factory([
            'link_id' => $link->id
        ])->create();
        $this->assertInstanceOf(Link::class, $visit->link);
    }

    public function test_a_link_hasMany_visits()
    {
        $user = User::factory()->create();
        $link = Link::factory([
            'user_id' => $user->id
        ])->create();
        $visit = Visit::factory([
            'link_id' => $link->id
        ])->create();
        $this->assertTrue($link->visits->contains($visit));
    }

    public function test_visits_database_has_expected_columns()
    {
        $this->assertTrue(
            Schema::hasColumns('visits', [
                'id',
                'link_id',
                'user_ip',
                'user_agent'
            ]));
    }

}
