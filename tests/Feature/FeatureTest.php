<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class FeatureTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_route_home_via_web_routing()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }

    public function test_api_route_performShorten_must_show_200()
    {
        $response = $this->post('/shorten_link',["long_link" => "https://serargtg"]);

        $response->assertStatus(200);
    }


    ##notice:for running this test notice that ur database in .env file should be on link_ai_laravel_1 because it saves
    ##shorten_link in this database
    public function test_api_route_performShorten_must_see_shorted_link()
    {
        $response = $this->post('/shorten_link',["long_link" => "https://semaphoreci.com/community/tutorials/getting-started-with-phpunit-in-laraveleee"]);

        $response->assertSee('JxLktw');
    }

}
