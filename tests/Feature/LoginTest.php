<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;

use Tests\TestCase;



class LoginTest extends TestCase
{
    use WithFaker, RefreshDatabase;
    /**
     * User can login with valid email and password.
     *
     * @return void
     */
    public function test_user_can_login_successfully()
    {
        $user = User::factory()->create();
        $this->postJson(route('login'), [
            'email' => $user->email,
            'password' => '123456',
        ])
            ->assertStatus(Response::HTTP_OK)
            ->assertJsonStructure([
                'success',
                'token',
            ]);
    }

    public function test_user_cant_login_with_invalid_credentials()
    {
        $user = User::factory()->create();
        $this->postJson(
            route('login'),
            [
                'email' => 'invalid email',
                'password' => 'invalid password'
            ]
        )->assertStatus(Response::HTTP_UNAUTHORIZED)
            ->assertJsonStructure([
                'error'
            ]);
    }
}
