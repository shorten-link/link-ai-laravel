<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RegisterApiTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_user_can_register_successfully()
    {
        $this->postJson(route('register'), [
            'name' => $this->faker->name,
            'email' => $this->faker->email,
            'password' => '123456'
        ])
            ->assertStatus(Response::HTTP_CREATED)
            ->assertJsonStructure([
                'success',
                'message',
                'user'
            ]);
    }

    public function test_user_can_not_register_with_invalid_credentials()
    {
        $this->postJson(route('register'),
            [
                'name' => 'invalid name',
                'email' => 'invalid email',
                'password' => 'invalid password'
            ]
        )
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
