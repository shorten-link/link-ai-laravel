<?php

namespace Tests\Unit;

use App\Models\link;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Tests\TestCase;

class StoreLinkControllerTest extends TestCase
{
    use RefreshDatabase;

    public function test_unauthorized_user_get_403()
    {
        $this->postJson(route('links.store'))
            ->assertStatus(Response::HTTP_UNAUTHORIZED);
    }

    public function test_authorized_user_can_create_link()
    {
        $longLink = 'https://wwww.example.com';
        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(
                route('links.store'),
                [
                    'link' => $longLink
                ]
            )
            ->assertStatus(Response::HTTP_CREATED);

        $this->assertDatabaseHas(
            'links',
            [
                'link' => $longLink
            ]
        );
    }

    public function test_user_gets_422_when_link_not_present()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(
                route('links.store'),
            )
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(
                [
                    "message" => "The given data was invalid.",
                    "errors" => [
                        "link" => [
                            "The link field is required."
                        ]
                    ]
                ]
            );
    }

    public function test_user_gets_422_when_link_is_invalid()
    {
        $user = User::factory()->create();
        $this->actingAs($user)
            ->postJson(
                route('links.store'),
                [
                    'link' => 'I am not a link',
                ]
            )
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJson(
                [
                    "message" => "The given data was invalid.",
                    "errors" => [
                        "link" => [
                            "The link format is invalid."
                        ]
                    ]
                ]
            );
    }

//    public function test_link_exists_in_database_after_creation()
//    {
//        $user = User::factory()->create();
//        $link = new link();
//        $link->long_link = 'https://semaphoreci.com/community/tutorials/getting-started-with-phpunit-in-laravel';
//        $link->ip = '127.0.0.1';
//        $link->creator = 'PostmanRuntime/7.26.8';
//
//        $link->short_link='NZrLbX';
//
//        $link->save();
//
//        $search = link::where('short_link','NZrLbX')->get();
//
//        $this->assertNotNull($search);
//    }
//
//    public function test_count_of_url_after_creation()
//    {
//        link::factory(20)->create();
//
//        $count = link::count();
//
//        ##1 for upper test
//        $this->assertEquals(20,$count);
//
//    }
//
//    public function test_short_link_must_not_be_null_after_creation_via_factory()
//    {
//        $link = link::factory(1)->create();
//
//        $this->assertNotNull($link->first()->short_link);
//
//    }
//
//    public function test_count_of_short_link_letters_after_creation()
//    {
//        $link = link::factory(1)->create();
//        $short_link = $link->first();
//        $num = Str::length($short_link->short_link);
//        $this->assertEquals(6,$num);
//    }

}
