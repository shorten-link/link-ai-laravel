<?php

namespace Tests\Feature;

use App\Models\Link;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Http\Response;
use Tests\TestCase;

class RedirectControllerTest extends TestCase
{
    use WithFaker;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_response_can_redirect_successfully()
    {
        $user = User::factory()->create();
        $link = Link::factory([
            'user_id' => $user->id
        ])->create();
        $this->call('get', route(
            'short_link', $link->short_link
        ))->assertRedirect($link->link)
            ->assertStatus(Response::HTTP_FOUND);
    }

    public function test_link_not_found()
    {
        $this->call('get', route(
            'short_link', 'invalid_short_link'
        ))->assertStatus(Response::HTTP_NOT_FOUND);
    }

}
