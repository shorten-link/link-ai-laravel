<?php

namespace Tests\Feature;

use App\Http\Resources\LinkResource;
use App\Models\Link;
use App\Models\User;
use App\Models\Visit;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Response;
use Tests\TestCase;

class LinkResourceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_get_all_links()
    {
        $user = User::factory()->create();
        $links = Link::factory(2)->create(['user_id' => $user->id]);
        foreach ($links as $link) {
            Visit::factory(2)->create(['link_id' => $link->id]);
        }
        $this->actingAs($user)->get(route('links.index'))
            ->assertStatus(Response::HTTP_OK);
    }

    public function test_resource_of_link_and_visit_and_the_response_is_equal()
    {
        $user = User::factory()->create();
        $links = Link::factory(2)->create(['user_id' => $user->id]);
        foreach ($links as $link) {
            Visit::factory(2)->create(['link_id' => $link->id]);
        }
        $links = Link::where('user_id', $user->id)->withCount('visits')->get();
        $response = $this->actingAs($user)->get(route('links.index'));
        $json = $response->json();
        $resource = LinkResource::collection($links);
        $resourceResponse = $resource->response()->getData(true);
        $this->assertEquals($json, $resourceResponse);
    }

    public function test_linkResource_has_these_parameters()
    {
        $user = User::factory()->create();
        $links = Link::factory(3)->create(['user_id' => $user->id]);
        foreach ($links as $link) {
            Visit::factory(2)->create(['link_id' => $link->id]);
        }
        $res = $this->actingAs($user)->getJson(route('links.index'))
            ->assertStatus(Response::HTTP_OK)
            ->assertSuccessful()
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'short_link',
                        'link',
                        'ip',
                        'visits'
                    ]
                ]
            ]);
    }
}
