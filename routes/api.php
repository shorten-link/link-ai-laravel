<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\LinkController;
use App\Http\Controllers\RedirectLinkController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', [AuthController::class, 'register'])->name('register');
Route::post('login', [AuthController::class, 'login'])->name('login');

/* shorten link api */

Route::middleware('auth:api')->group(function () {
    Route::apiResource('links', LinkController::class)->only('store','index');
    Route::post('logout', [AuthController::class,'logout'])->name('logout');
    Route::get('me', [AuthController::class,'me'])->name('me');
});

Route::get('{shortLink}',[RedirectLinkController::class,'redirect'])->name('short_link');
